SUMMARY
=======
This is the DuMuX module containing the code relevant to produce the results for:

 Becker, B., Guo, B., Bandilla, K., Celia, M. A., Flemisch, B., & Helmig, R. (2017).<br> 
 A pseudo-vertical equilibrium model for slow gravity drainage dynamics.<br> 
 Water Resources Research, 53, 10,491–10,507. https://doi.org/10.1002/2017WR021644 

Installation
============

The easiest way to install the program (DuMuX and DUNE) with this module is to execute the file
[installBecker2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Becker2017a/raw/master/installBecker2017a.sh)
in the directory where you want to install the program.

```bash
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Becker2017a/raw/master/installBecker2017a.sh
sh ./installBecker2017a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The applications used for this publication can be found in the build directory under

test/decoupled/2pve (pseudo-VE models and conventional VE models),<br>
appl/VEmod/twoD (two-dimensional reference).

You can run the executable with:
```bash
./test_2pve
```


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installBecker2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Becker2017a/raw/master/installBecker2017a.sh).

In addition the following external software packages are necessary for compiling the executables: cmake, SuperLU.

