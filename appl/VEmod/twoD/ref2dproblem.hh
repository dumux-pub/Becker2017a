/*****************************************************************************
 *   Copyright (C) 2007-2008 by Klaus Mosthaf                                *
 *   Copyright (C) 2007-2008 by Bernd Flemisch                               *
 *   Copyright (C) 2008-2009 by Andreas Lauser                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef REF_TWOD_PROBLEM_HH
#define REF_TWOD_PROBLEM_HH

#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif

#include <dune/common/float_cmp.hh>
#include <dumux/io/gnuplotinterface.hh>

#include "../h2o.hh"
#include "../ch4.hh"

//#include <dumux/porousmediumflow/2p/sequential/diffusion/cellcentered/pressurepropertiesadaptive.hh>
#include <dumux/porousmediumflow/2p/sequential/diffusion/cellcentered/pressureproperties.hh>
#include <dumux/porousmediumflow/2p/sequential/transport/cellcentered/properties.hh>
#include <dumux/porousmediumflow/2p/sequential/impes/problem.hh>
#include <dumux/porousmediumflow/2p/sequential/transport/cellcentered/evalcflfluxcoats.hh>
#include <dumux/porousmediumflow/sequential/gridadapt.hh>

#include "ref2dspatialparams.hh"

namespace Dumux {

template<class TypeTag>
class Ref2DStorageProblem;

//////////
// Specify the properties for the lens problem
//////////
namespace Properties {
NEW_TYPE_TAG(Ref2DStorageProblem, INHERITS_FROM(FVPressureTwoP, FVTransportTwoP, IMPESTwoP, Ref2DSpatialParams));

// Set the grid type
SET_TYPE_PROP(Ref2DStorageProblem, Grid, Dune::YaspGrid<2>);
//SET_TYPE_PROP(Ref2DStorageProblem, Grid, Dune::UGGrid<2>);

// Set the problem property
SET_TYPE_PROP(Ref2DStorageProblem, Problem, Dumux::Ref2DStorageProblem<TypeTag>);

// Set the wetting phase
SET_PROP(Ref2DStorageProblem, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef FluidSystems::LiquidPhase<Scalar, Dumux::H2O<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(Ref2DStorageProblem, NonwettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef FluidSystems::GasPhase<Scalar, Dumux::CH4<Scalar> > type;
};

SET_TYPE_PROP(Ref2DStorageProblem, LinearSolver, Dumux::SuperLUBackend<TypeTag>);

// Enable gravity
SET_BOOL_PROP(Ref2DStorageProblem, ProblemEnableGravity, true);
//SET_TYPE_PROP(Ref2DStorageProblem, EvalCflFluxFunction, Dumux::EvalCflFluxCoats<TypeTag>);
SET_BOOL_PROP(Ref2DStorageProblem, EnableCompressibility, false);

//Set the grid adaption model
//SET_TYPE_PROP(Ref2DStorageProblem,  GridAdaptModel, Dumux::GridAdapt<TypeTag, true>);
}
/*!
 * \ingroup DecoupledProblems
 */
template<class TypeTag>
class Ref2DStorageProblem: public IMPESProblem2P<TypeTag> {
    typedef IMPESProblem2P<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView)GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) WettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, NonwettingPhase) NonWettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        pwIdx = Indices::pwIdx,
        swIdx = Indices::swIdx,
        pressEqIdx = Indices::pressureEqIdx,
        satEqIdx = Indices::satEqIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename GET_PROP_TYPE(TypeTag, CellData) CellData;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP(TypeTag, SolutionTypes)::PrimaryVariables PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;

    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename SpatialParams::MaterialLaw MaterialLaw;

    typedef std::array<int, dim> CellArray;

public:
    Ref2DStorageProblem(TimeManager& timeManager, const GridView &gridView) :
    ParentType(timeManager, gridView)
    {
//        this->setGrid(GridCreator::grid());
//        GridCreator::grid().globalRefine(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, GridAdapt, MaxLevel));

        int outputInterval = 1e9;
        if (ParameterTree::tree().hasKey("Problem.OutputInterval"))
        {
            outputInterval = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OutputInterval);
        }
        this->setOutputInterval(outputInterval);

        if (ParameterTree::tree().hasKey("Problem.OutputTimeInterval"))
        {
            Scalar outputTimeInterval = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OutputTimeInterval);
            this->setOutputTimeInterval(outputTimeInterval);
        }

        // store pointer to all elements in a multimap, elements belonging to the same column
        // have the same key, starting with key = 0 for first column
        // TODO: only works for equidistant grids
        //
        // iterate over all elements
        int j = 0;
        for (const auto& element : Dune::elements(this->gridView()))
        {
            // identify column number
            GlobalPosition globalPos = element.geometry().center();
            CellArray numberOfCellsX = GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, CellArray, "Grid", Cells);
            double deltaX = this->bBoxMax()[0]/numberOfCellsX[0];

            j = round((globalPos[0] - (deltaX/2.0))/deltaX);

            mapColumns_.insert(std::make_pair(j, element));
            dummy_ = element;
        }
        averageSatPlume_.resize(j+1);
        pseudoResSatExpl_ = 1.0;

        //calculate length of capillary transition zone (CTZ)
        Scalar swr = this->spatialParams().materialLawParams(dummy_).swr();
        Scalar snr = this->spatialParams().materialLawParams(dummy_).snr();
        Scalar satW1 = 1.0 - snr;
        Scalar satW2 = swr + 0.1*(1.0-swr-snr);
        Scalar pc1 = MaterialLaw::pc(this->spatialParams().materialLawParams(dummy_), satW1);
        Scalar pc2 = MaterialLaw::pc(this->spatialParams().materialLawParams(dummy_), satW2);
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar pRef = referencePressureAtPos(globalPos);
        Scalar tempRef = temperatureAtPos(globalPos);
        Scalar densityW = WettingPhase::density(tempRef, pRef);
        Scalar densityN = NonWettingPhase::density(tempRef, pRef);
        Scalar gravity = this->gravity().two_norm();
        CTZ_ = (pc2-pc1)/((densityW-densityN)*gravity);
        std::cout << "CTZ " << CTZ_ << std::endl;

        Scalar height = this->bBoxMax()[dim-1];
        Scalar porosity = this->spatialParams().porosity(dummy_);
        Scalar viscosityW = WettingPhase::viscosity(tempRef, pRef);
        Scalar permeability = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, PermeabilityVertical);
        segTime_ = (height*porosity*viscosityW)/(permeability*gravity*(densityW-densityN));
        std::cout << "segTime " << segTime_ << std::endl;

        outputFile_.open("averageSatPlume.out", std::ios::trunc);
        outputFile_.close();

        outputFile_.open("satScatter.out", std::ios::trunc);
        outputFile_.close();

        outputFile_.open("plumeTip.out", std::ios::trunc);
        outputFile_.close();
    }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        ParentType::init();

        bool plotFluidMatrixInteractions = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotFluidMatrixInteractions);

        // plot the Pc-Sw curves, if requested
        if(plotFluidMatrixInteractions)
            this->spatialParams().plotMaterialLaw();
    }

    /*!
     * \name Problem parameters
     */
// \{
    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        if (ParameterTree::tree().hasKey("Problem.Name"))
        {
            std::string fileName(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name));
            return fileName.c_str();
        }
        else
        {
            return "test_impes2p";
        }
    }
    bool shouldWriteRestartFile() const
    {
        return false;
    }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 326.0; // -> 41,85°C
    }

// \}

    Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
    {
        return 1.0e7; //
    }

    void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition& globalPos) const
    {
        if (globalPos[0] > this->bBoxMax()[0] - eps_)
        {
            bcTypes.setAllDirichlet();
        }
        // all other boundaries
        else
        {
            bcTypes.setAllNeumann();
        }
    }

    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        values = 0;

        if (globalPos[0] > this->bBoxMax()[0] - eps_)
        {
            if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
            {
                Scalar pRef = referencePressureAtPos(globalPos);
                Scalar temp = temperatureAtPos(globalPos);

                values[pwIdx] = (pRef + (this->bBoxMax()[dim-1] - globalPos[dim-1])
                                       * WettingPhase::density(temp, pRef)
                                       * this->gravity().two_norm());
            }
            else
            {
                values[pwIdx] = 1.0e7;
            }
            values[swIdx] = 1.0;
        }
        else
        {
            values[pwIdx] = 1.0e7;
            values[swIdx] = 0.0;
        }
    }

//! set neumann condition for phases (flux, [kg/(m^2 s)])
    void neumannAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        values = 0.0;
        if (globalPos[0] < eps_)
        {
            values[nPhaseIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, BoundaryConditions, Injectionrate);
        }
    }

    void source(PrimaryVariables &values, const Element& element) const
    {
        values = 0.0;
    }

    void initialAtPos(PrimaryVariables &values,
            const GlobalPosition& globalPos) const
    {
        values[pressEqIdx] = 1.0e7;
        values[swIdx] = 1.0;
    }

    void postTimeStep()
    {
        ParentType::postTimeStep();

        CellArray numberOfCells = GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, CellArray, "Grid", Cells);
        double deltaX = this->bBoxMax()[0]/numberOfCells[0];
//        double deltaZ = this->bBoxMax()[dim-1]/numberOfCells[dim-1];

        int outputInterval = 1e9;
        if (ParameterTree::tree().hasKey("Problem.OutputInterval"))
        {
            outputInterval = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OutputInterval);
        }

        if(this->timeManager().timeStepIndex() % outputInterval == 0 || this->timeManager().willBeFinished()
                || this->timeManager().episodeWillBeFinished() || this->timeManager().timeStepIndex() == 0)
        {
            // initialize/reset average column saturation
//            for (int i = 0; i < averageSatPlume_.size(); i++)
//            {
//                averageSatPlume_[i] = 0.0;
//            }
//
//            const GlobalPosition globalPos = dummy_.geometry().center();
//            const Scalar pRef = referencePressureAtPos(globalPos);
//            const Scalar temp = temperatureAtPos(globalPos);
//            const Scalar aquiferHeight = this->bBoxMax()[1];
//            const Scalar porosity = this->spatialParams().porosity(dummy_);
//            const Scalar viscosityW = WettingPhase::viscosity(temp, pRef);
//            const Scalar viscosityNw = NonWettingPhase::viscosity(temp, pRef);
            Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
//            const Scalar permeability = this->spatialParams().intrinsicPermeability(dummy_);
//            const Scalar densityW = WettingPhase::density(temp, pRef);
//            const Scalar densityN = NonWettingPhase::density(temp, pRef);
//            const Scalar gravity = this->gravity().two_norm();
//            const Scalar lambda = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Lambda);
//            const Scalar resSatW = this->spatialParams().materialLawParams(dummy_).swr();
//            const Scalar resSatN = this->spatialParams().materialLawParams(dummy_).snr();
//            const int spatialModel = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, Model);
//            const Scalar entryP = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, EntryPressure);
//
//            Scalar averageSatTotal = 0.0; // average sat in plume in domain (if applicable not including capillary fringe)
//            Scalar gasPlumeVolume = 0.0; // volume of gas plume (if applicable not including capillary fringe)
//            Scalar gasPlumeTip = 0.0; // distance of gas plume tip from injection
//            Scalar averagePlumeHeight = 0.0; // average gas plume height, averaged over all columns with gas (not including capillary fringe)
//            Scalar averagePlumeHeightSeg = 0.0; // average gas plume height assuming segregation, averaged over all columns with gas (not including capillary fringe)
//            Scalar corrGasPlumeHeightSeg = 0.0; // average gas plume height assuming segregation, averaged over all columns with gas (not including capillary fringe),
//                                                // including a pseudo residual saturation inside plume
//            Scalar totalGasColumnVolume = 0.0; // sum of total column volume with gas
//            for (int i = 0; i != averageSatPlume_.size(); ++i)
//            {
//                Scalar totalVolume = 0.0;// total volume of column
//                Scalar gasVolume = 0.0;// volume with SatN>0.0 in one column;
//                Scalar averageSat = 0.0;// average sat in column;
//                Scalar CTZStart = 1.0e100;//distance from
//                bool gasInsideColumn = 0;// is there gas inside this column?
//                typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(i);
//                for (; it != mapColumns_.upper_bound(i); ++it)
//                {
//                    int globalIdxI = this->variables().index(it->second);
//                    Scalar satW = this->variables().cellData(globalIdxI).saturation(wPhaseIdx);
//                    Scalar satNw = this->variables().cellData(globalIdxI).saturation(nPhaseIdx);
//                    Scalar volume = it->second.geometry().volume();
//                    totalVolume += volume;
//                    averageSat += satW * volume;
//                    if(satNw>eps_)
//                    {
//                        gasInsideColumn = 1;
//                        averageSatPlume_[i] += satW * volume;
//                        gasVolume += volume;
//                        GlobalPosition globalPos = it->second.geometry().center();
//                        CTZStart = std::min(CTZStart, globalPos[dim-1]-deltaZ/2.0);//column is traversed from bottom to top
//                        if(globalPos[dim-1]-deltaZ/2.0 >= CTZStart+CTZ_)
//                        {
//                            averageSatTotal += satW * volume;
//                            gasPlumeVolume += volume;
//                        }
//                        if(this->timeManager().timeStepIndex() == 10000)
//                        {
//                            outputFile_.open("satScatter.out", std::ios::app);
//                            outputFile_ << i << " " << satW << std::endl;
//                            outputFile_.close();
//                        }
//                    }
//                }
//                if(gasVolume < eps_)
//                    gasVolume = 0.0;
//                if(gasPlumeVolume < eps_)
//                    gasPlumeVolume = 0.0;
//                if(gasInsideColumn)
//                {
//                    CellArray numberOfCellsX = GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, CellArray, "Grid", Cells);
//                    gasPlumeTip = std::max(gasPlumeTip, (i+1)*deltaX);
//                    if(aquiferHeight - (CTZStart + CTZ_) > 0.0)//otherwise all is capillary fringe
//                        averagePlumeHeight += (aquiferHeight - (CTZStart + CTZ_)) * totalVolume;
//                    averageSat = averageSat / totalVolume;
//                    totalGasColumnVolume += totalVolume;
//                    Scalar resSatWCorr = pseudoResSatExpl_;
//                    if(spatialModel == 2 && entryP > 0.0+eps_) //calculate averagePlumeHeight for capillary fringe Brooks-Corey model
//                    {
//                        Scalar fullIntegral = 1.0 / (1.0 - lambda) * (1.0 - resSatW - resSatN) / ((densityW - densityN) * gravity) * (std::pow(entryP, lambda)
//                        - std::pow(entryP, 2.0 - lambda) + std::pow((aquiferHeight * (densityW - densityN) * gravity + entryP), (1.0 - lambda)));
//                        //calculate distance of plume from bottom, Xi
//                        Scalar Xi = aquiferHeight / 2.0; //XiStart
//                        if(fullIntegral < averageSat * aquiferHeight) //GasPlumeDist>0
//                        {
//                            for (int count = 0; count < 100; count++)
//                            {
//                                Scalar residual = 1.0 / (1.0 - lambda) * std::pow(((aquiferHeight - Xi) * (densityW - densityN) * gravity + entryP),(1.0 - lambda))
//                                * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity) + resSatW * (aquiferHeight - Xi)
//                                - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity) + Xi - averageSat * aquiferHeight;
//
//                                if (fabs(residual) < 1e-10)
//                                    break;
//
//                                Scalar derivation = std::pow(((aquiferHeight - Xi) * (densityW - densityN) * gravity + entryP), -lambda) * (resSatN + resSatW - 1.0)
//                                        * std::pow(entryP, lambda) - resSatW + 1.0;
//
//                                Xi = Xi - residual / (derivation);
//                            }
//                        }
//                        else //GasPlumeDist<=0
//                        {
//                            for (int count = 0; count < 100; count++)
//                            {
//                                Scalar residual = 1.0 / (1.0 - lambda) * std::pow(((aquiferHeight - Xi) * (densityW - densityN) * gravity + entryP),
//                                        (1.0 - lambda)) * (1.0 - resSatW - resSatN)* std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
//                                        + resSatW * aquiferHeight - 1.0 / (1.0 - lambda) * std::pow(((-Xi) * (densityW - densityN)
//                                        * gravity + entryP),(1.0 - lambda)) * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN)
//                                                * gravity)
//                                                - averageSat * aquiferHeight;
//
//                                if (fabs(residual) < 1e-10)
//                                    break;
//
//                                Scalar derivation = std::pow(((aquiferHeight - Xi) * (densityW - densityN) * gravity + entryP), -lambda)
//                                * (resSatN + resSatW - 1.0) * std::pow(entryP, lambda) + std::pow(((-Xi) * (densityW - densityN) * gravity + entryP),
//                                        -lambda) * (1.0 - resSatN - resSatW) * std::pow(entryP, lambda);
//
//                                Xi = Xi - residual / (derivation);
//                            }
//                        }
//                        if(Xi + CTZ_ < 0.0)//all of column is gas plume not including capillary fringe
//                            averagePlumeHeightSeg += aquiferHeight * totalVolume;
//                        else if((aquiferHeight - (Xi + CTZ_)) > 0.0)//otherwise all of column is capillary fringe
//                        {
//                            averagePlumeHeightSeg += (aquiferHeight - (Xi + CTZ_)) * totalVolume;
//                        }
//
//                        //calculate gasPlumeHeight for a model with pseudo residual saturation
//                        fullIntegral = 1.0 / (1.0 - lambda) * (1.0 - resSatWCorr - resSatN) / ((densityW - densityN) * gravity) * (std::pow(entryP, lambda)
//                        - std::pow(entryP, 2.0 - lambda) + std::pow((aquiferHeight * (densityW - densityN) * gravity + entryP), (1.0 - lambda)));
//                        //calculate distance of plume from bottom, Xi
//                        Xi = aquiferHeight / 2.0; //XiStart
//                        if(fullIntegral < averageSat * aquiferHeight) //GasPlumeDist>0
//                        {
//                            for (int count = 0; count < 100; count++)
//                            {
//                                Scalar residual = 1.0 / (1.0 - lambda) * std::pow(((aquiferHeight - Xi) * (densityW - densityN) * gravity + entryP),(1.0 - lambda))
//                                * (1.0 - resSatWCorr - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity) + resSatWCorr * (aquiferHeight - Xi)
//                                - entryP * (1.0 - resSatWCorr - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity) + Xi - averageSat * aquiferHeight;
//
//                                if (fabs(residual) < 1e-10)
//                                    break;
//
//                                Scalar derivation = std::pow(((aquiferHeight - Xi) * (densityW - densityN) * gravity + entryP), -lambda) * (resSatN + resSatWCorr - 1.0)
//                                        * std::pow(entryP, lambda) - resSatWCorr + 1.0;
//
//                                Xi = Xi - residual / (derivation);
//                            }
//                        }
//                        else //GasPlumeDist<=0
//                        {
//                            for (int count = 0; count < 100; count++)
//                            {
//                                Scalar residual = 1.0 / (1.0 - lambda) * std::pow(((aquiferHeight - Xi) * (densityW - densityN) * gravity + entryP),
//                                        (1.0 - lambda)) * (1.0 - resSatWCorr - resSatN)* std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
//                                        + resSatWCorr * aquiferHeight - 1.0 / (1.0 - lambda) * std::pow(((-Xi) * (densityW - densityN)
//                                        * gravity + entryP),(1.0 - lambda)) * (1.0 - resSatWCorr - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN)
//                                                * gravity)
//                                                - averageSat * aquiferHeight;
//
//                                if (fabs(residual) < 1e-10)
//                                    break;
//
//                                Scalar derivation = std::pow(((aquiferHeight - Xi) * (densityW - densityN) * gravity + entryP), -lambda)
//                                * (resSatN + resSatWCorr - 1.0) * std::pow(entryP, lambda) + std::pow(((-Xi) * (densityW - densityN) * gravity + entryP),
//                                        -lambda) * (1.0 - resSatN - resSatWCorr) * std::pow(entryP, lambda);
//
//                                Xi = Xi - residual / (derivation);
//                            }
//                        }
//                        if(Xi + CTZ_ < 0.0)//all of column is gas plume not including capillary fringe
//                            corrGasPlumeHeightSeg+= aquiferHeight * totalVolume;
//                        else if((aquiferHeight - (Xi + CTZ_)) > 0.0)//otherwise all of column is capillary fringe
//                        {
//                            corrGasPlumeHeightSeg+= (aquiferHeight - (Xi + CTZ_)) * totalVolume;
//                        }
//                    }
//                    else //calculate averagePlumeHeight for sharp interface model
//                    {
//                        averagePlumeHeightSeg += aquiferHeight * (averageSat - resSatW) / (1.0 - resSatW) * totalVolume;
//                        corrGasPlumeHeightSeg += aquiferHeight * (averageSat - resSatWCorr) / (1.0 - resSatWCorr) * totalVolume;
//                    }
//                }
//                averageSatPlume_[i] = averageSatPlume_[i]/gasVolume;
//                outputFile_.open("averageSatPlume.out", std::ios::app);
//                outputFile_ << " " << averageSatPlume_[i];
//                outputFile_.close();
//            }
//            averagePlumeHeight = averagePlumeHeight/totalGasColumnVolume;
//            averagePlumeHeightSeg = averagePlumeHeightSeg/totalGasColumnVolume;
//            corrGasPlumeHeightSeg= corrGasPlumeHeightSeg/totalGasColumnVolume;
//            if(isnan(averagePlumeHeight))
//                averagePlumeHeight = 0.0;
//            if(isnan(averagePlumeHeightSeg))
//                averagePlumeHeightSeg = 0.0;
//            if(isnan(corrGasPlumeHeightSeg))
//                corrGasPlumeHeightSeg= 0.0;
//
//            // calculate average wetting phase saturation in plume for whole domain by averaging over all cells (excluding capillary frigne)
//            averageSatTotal = averageSatTotal/gasPlumeVolume;
//            if(isnan(averageSatTotal))
//                averageSatTotal = 0.0;
//
//            //calculate pseudo residual saturation based on approximative equation
//            //1.) with h = height of domain
//            //2.) with h = globally average height h from last time step, assuming full segregation and not including capillary fringe
//            //3.) with h = globally average height h from analytic solution, assuming sharp interface
//            Scalar gasVolume = -GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, Scalar, "BoundaryConditions", Injectionrate) *
//                    aquiferHeight * time / (densityN);
//            Scalar satN = gasVolume/(aquiferHeight * gasPlumeTip * porosity);//average sat for volume with gas columns
//
//            int numberOfApproaches = 3;
//            Scalar pseudoResSat[numberOfApproaches] = {};
//            Scalar height[numberOfApproaches] = {};
//            height[0] = aquiferHeight;
//            height[1] = averagePlumeHeightSeg;
//            height[2] = viscosityNw/viscosityW * aquiferHeight;
//
//            Scalar exponent = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, Exponent);
//
//            for(int i=0; i != numberOfApproaches; ++i)
//            {
//                if (spatialModel == 0)//for linear relative permeability model
//                {
//                    pseudoResSat[i] = height[i]*porosity*viscosityW/(time*permeability*(densityW-densityN)*gravity) * (1-resSatW-resSatN) + resSatW;
//                }
//                else if (spatialModel == 1)//for exponential relative permeability model
//                {
//                    pseudoResSat[i] = std::pow(height[i]*porosity*viscosityW/(time*permeability*(densityW-densityN)*gravity), 1/exponent)
//                    * (1-resSatW-resSatN) + resSatW;
//                }
//                else//for Brooks-Corey relative permeability model
//                {
//                    pseudoResSat[i] = std::pow(height[i]*porosity*viscosityW/(time*permeability*(densityW-densityN)*gravity), 1.0/((2.0/lambda) + 3.0))
//                    * (1-resSatW-resSatN) + resSatW;
//                }
//
//                if(isnan(pseudoResSat[i]) || isinf(pseudoResSat[i]))
//                    pseudoResSat[i] = 0.0;
//            }
//
//            outputFile_.open("averageSatPlume.out", std::ios::app);
//            outputFile_ << " " << std::endl;
//            outputFile_.close();
//
//            //calculate sw explicitly
//            Scalar timeStepSize = this->timeManager().timeStepSize();
//            if(averagePlumeHeight> aquiferHeight)
//                averagePlumeHeight= aquiferHeight;
//            Scalar drainageFlux = MaterialLaw::krw(this->spatialParams().materialLawParams(*dummy_), pseudoResSatExpl_)/
//                    viscosityW*permeability*gravity*(densityW-densityN);
//            if(drainageFlux > averagePlumeHeight* pseudoResSatExpl_ * (1.0-pseudoResSatExpl_) * porosity/timeStepSize)
//            {
//                std::cout << "attention: time step too large!!" << std::endl;
//                drainageFlux = averagePlumeHeight* (1.0-pseudoResSatExpl_) *pseudoResSatExpl_ * porosity/timeStepSize;
//            }
//            pseudoResSatExpl_ = (averagePlumeHeight * porosity * pseudoResSatExpl_ - drainageFlux * timeStepSize/(1.0-pseudoResSatExpl_))
//                    / (averagePlumeHeight * porosity - drainageFlux * timeStepSize / (1.0-pseudoResSatExpl_));
//
//            if(this->timeManager().timeStepIndex() <= 11)//no segregation in first 11 time steps
//            {
//                pseudoResSatExpl_ = averageSatTotal;
//            }
//
//            std::cout << "averagePlumeHeight " << averagePlumeHeight<< std::endl;
//            std::cout << "drainageFlux " << drainageFlux << std::endl;
//            std::cout << "pseudoResSatExpl_ " << pseudoResSatExpl_ << std::endl;
//            std::cout << "firstTerm " << averagePlumeHeight* porosity * pseudoResSatExpl_ << std::endl;
//            std::cout << "secondTerm " <<  drainageFlux * timeStepSize/(1.0-pseudoResSatExpl_) << std::endl;
//            std::cout << "thirdTerm " <<  averagePlumeHeight* porosity << std::endl;
//            std::cout << "fourthTerm " <<  drainageFlux * timeStepSize / (1.0-pseudoResSatExpl_) << std::endl;
//
//
//            static double yMax = -1e9;
//            static std::vector<double> x;
//            static std::vector<double> y;
//            static std::vector<double> y2;
//            static std::vector<double> y3;
//            static std::vector<double> y4;
//            static std::vector<double> y5;
//            static std::vector<double> y6;
//            static std::vector<double> y7;
//
//            x.push_back(time/segTime_);
//            y.push_back(averageSatTotal);
//            y2.push_back(pseudoResSat[0]);
//            y3.push_back(pseudoResSat[1]);
//            y4.push_back(pseudoResSat[2]);
//            y5.push_back(pseudoResSatExpl_);
//
//            yMax = std::max({yMax, 1.5});
//
//            gnuplot_.reset();
//            gnuplot_.setXRange(0, x.back()+0.1);
//            gnuplot_.setYRange(0.0, yMax);
//            gnuplot_.setXlabel("tsim/tseg [-]");
//            gnuplot_.setYlabel("Saturation [-]");
//            if(this->timeManager().timeStepIndex() > 0)
//            {
//                gnuplot_.addDataSetToPlot(x, y, "sat (excluding CF if present)", "axes x1y1 w l");
//                gnuplot_.addDataSetToPlot(x, y2, "pseudo-sat-H", "axes x1y1 w l");
//                gnuplot_.addDataSetToPlot(x, y3, "pseudo-sat-averageH (excluding CF if present)", "axes x1y1 w l");
//                gnuplot_.addDataSetToPlot(x, y4, "pseudo-sat-analyticH", "axes x1y1 w l");
//                gnuplot_.addDataSetToPlot(x, y5, "pseudoResSatExpl_", "axes x1y1 w l");
//
//                gnuplot_.plot("Sat-in-plume");
//            }

            // calculate distance of gas plume from injection if things above are commented out
            Scalar gasPlumeTip = 0.0;
            GridView GridView = this->gridView();
            for (const auto& element : Dune::elements(GridView))
            {
                GlobalPosition globalPos = element.geometry().center();
                CellArray numberOfCellsX = GET_RUNTIME_PARAM_FROM_GROUP_CSTRING(TypeTag, CellArray, "Grid", Cells);
                int j = round((globalPos[0] - (deltaX/2.0))/deltaX);
                int eIdxGlobal = this->variables().index(element);
                CellData& cellData = this->variables().cellData(eIdxGlobal);
                Scalar satW = cellData.saturation(wPhaseIdx);
                if(satW < (1.0-eps_))
                {
                    gasPlumeTip = std::max(gasPlumeTip, (j+1)*deltaX);
                }
            }

            //write out distance of gas plume from injection
            outputFile_.open("plumeTip.out", std::ios::app);
            outputFile_ << time << " " << gasPlumeTip << std::endl;
            outputFile_.close();
        }
    }


private:
    static constexpr Scalar eps_ = 1e-6;
    static constexpr Scalar eps2_ = 1e-6;
    static constexpr Scalar depthBOR_ = 1000;

    std::multimap<int, Element> mapColumns_;
    std::vector<double> averageSatPlume_;
    std::ofstream outputFile_;
    Element dummy_;
    Scalar CTZ_;
    Scalar segTime_;

    Scalar pseudoResSatExpl_;

    Dumux::GnuplotInterface<double> gnuplot_;
};
}
 //end namespace

#endif
